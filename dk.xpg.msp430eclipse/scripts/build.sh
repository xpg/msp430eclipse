#!/bin/bash

# Tested on 
# - Fedora Core 13
# - Ubuntu 11.04


ACTION="$1"

case $ACTION in
	build)
	;;
	clean)
		exit 0
	;;
	*)
		echo "Unknown action"
		exit 1
	;;
esac

function printBuildUsage()
{
	echo "$0 build <version> [<toolchain-id-prefix> [<name>]]"
	exit 1
}

VERSION="$2"
TOOLCHAIN_ID_PREFIX="$3"
TOOLCHAIN_DESC="$4"

if [ "$TOOLCHAIN_ID_PREFIX" = "" ]; then
	TOOLCHAIN_ID_PREFIX="dk.xpg.toolchain"
fi
if [ "$TOOLCHAIN_DESC" = "" ]; then
	TOOLCHAIN_DESC="xpg.dk pre-build toolchain $VERSION"
fi

if [ "$VERSION" = "" -o "$TOOLCHAIN_ID_PREFIX" = "" -o "$TOOLCHAIN_DESC" = "" ]; then
	printBuildUsage
fi

ARCH="amd64"
OS="linux"
case $(uname -i) in
	x86_64)
	ARCH="amd64"
	;;
	i386)
	ARCH="i386"
	;;
esac
SCRIPT_HOME=$PWD
TOOLCHAIN_NAME="msp430-toolchain-${OS}-${ARCH}"
SCRIPT_HOME="$PWD"

SRC_ROOT="${SCRIPT_HOME}/src"
export MSP430_ROOT=$SRC_ROOT
mkdir -p $SRC_ROOT

TARBALL_ROOT="${SCRIPT_HOME}/tars"
mkdir -p $TARBALL_ROOT

INSTALL_DIR="${SCRIPT_HOME}/install/${TOOLCHAIN_NAME}"
BUILD_ROOT="${SCRIPT_HOME}/BUILD/${TOOLCHAIN_NAME}"
mkdir -p $BUILD_ROOT
mkdir -p $INSTALL_DIR

PACKAGE_DIR="${SCRIPT_HOME}/package/${TOOLCHAIN_NAME}-${VERSION}"

# Detect distribution
DISTRIBUTION=""


if [ -e "/etc/redhat-release" ]; then
	DISTRIBUTION="redhat"
elif [ -e "/etc/debian_version" ]; then
        DISTRIBUTION="debian"
fi


if [ "$DISTRIBUTION" == "" ]; then
	echo "Unknown distribution"
	exit 1
fi

function ensureInstalled ()
{
	local PACKAGE=$1

	if [ "$DISTRIBUTION" == "redhat" ]; then
		rpm -ql $PACKAGE > /dev/null
		if [ $? -ne 0 ]; then
			sudo yum install $PACKAGE
		fi
	fi
	if [ "$DISTRIBUTION" == "debian" ]; then
		dpkg -L $PACKAGE > /dev/null
		if [ $? -ne 0 ]; then
			sudo apt-get install $PACKAGE
		fi
	fi
}

if [ "$DISTRIBUTION" == "redhat" ]; then
	ensureInstalled gmp-devel
	ensureInstalled mpfr-devel
	ensureInstalled libmpc-devel
elif [ "$DISTRIBUTION" == "debian" ]; then
	ensureInstalled libgmp3-dev
	ensureInstalled libmpfr-dev
	ensureInstalled libmpc-dev
fi


function gitUpdate()
{
	local SRC_ROOT=$1
	local DIR=$2
	local URL=$3
	if [ -e "$DIR" ]; then
		cd $DIR
		echo "Updating $DIR..."
		git pull
	else
		cd $SRC_ROOT
		git clone "$URL" "$DIR"
        fi
	cd $DIR
	git log -1 | grep commit > git-version
}

function fetch()
{
	local DOWNLOAD_DIR="$1"
	local EXTRACT_DIR="$2"
	local FILENAME="$3"
	local URL="$4"
	
	if [ ! -e "$DOWNLOAD_DIR/$FILENAME" ]; then
		pushd $DOWNLOAD_DIR
		wget "$URL"
		popd
	fi
	
	pushd $EXTRACT_DIR
	tar jxvf $DOWNLOAD_DIR/$FILENAME
	popd
}

function generate_wrapper()
{
	local TEMPLATE="$1"
	local WRAPPER="$2"
	local EXEC="$3"
	cat $SCRIPT_HOME/$TEMPLATE | sed -e "s/%EXEC%/$EXEC/g" > $WRAPPER
	chmod +x $WRAPPER
}

BINUTILS_DIR="${SRC_ROOT}/binutils"
GCC_DIR="${SRC_ROOT}/gcc"
MSP430_LIBC_DIR="${SRC_ROOT}/msp430-libc"
MSP430_MCU_SOURCE="http://sourceforge.net/projects/mspgcc/files/msp430mcu/msp430mcu-20130321.tar.bz2"
MSP430_MCU_DIR="${SRC_ROOT}/msp430mcu-20130321"
MSPGCC_DIR="${SRC_ROOT}/mspgcc"
MSPDEBUG_DIR="${SRC_ROOT}/mspdebug"
MSP430_GDB_DIR="${SRC_ROOT}/gdb-7.2"

echo "Checkout root is $SRC_ROOT"

GDB_SOURCE="http://ftpmirror.gnu.org/gdb/gdb-7.2a.tar.bz2"

fetch "$TARBALL_ROOT" "$SRC_ROOT" "gdb-7.2a.tar.bz2" $GDB_SOURCE
#fetch "$TARBALL_ROOT" "$SRC_ROOT" "mspgcc-20120311.tar.bz2" "http://sourceforge.net/projects/mspgcc/files/mspgcc/mspgcc-20120311.tar.bz2"
gitUpdate "$SRC_ROOT" "$BINUTILS_DIR" "git://mspgcc.git.sourceforge.net/gitroot/mspgcc/binutils"
gitUpdate "$SRC_ROOT" "$GCC_DIR" "git://mspgcc.git.sourceforge.net/gitroot/mspgcc/gcc"
gitUpdate "$SRC_ROOT" "$MSP430_LIBC_DIR" "git://mspgcc.git.sourceforge.net/gitroot/mspgcc/msp430-libc"
#gitUpdate "$SRC_ROOT" "$MSP430_MCU_DIR" "git://mspgcc.git.sourceforge.net/gitroot/mspgcc/msp430mcu"
fetch "$TARBALL_ROOT" "$SRC_ROOT" "msp430mcu-20130321.tar.bz2" "$MSP430_MCU_SOURCE"
gitUpdate "$SRC_ROOT" "$MSPGCC_DIR" "git://mspgcc.git.sourceforge.net/gitroot/mspgcc/mspgcc"
gitUpdate "$SRC_ROOT" "$MSPDEBUG_DIR" "git://mspdebug.git.sourceforge.net/gitroot/mspdebug/mspdebug"

mkdir -p $INSTALL_DIR

mkdir -p $BUILD_ROOT/binutils/dev
cd $BUILD_ROOT/binutils/dev
$BINUTILS_DIR/configure \
  --target=msp430 \
  --prefix=${INSTALL_DIR} \
  2>&1 | tee co
make 2>&1 | tee mo
make check-gas RUNTESTFLAGS=msp430.exp
make install 2>&1 | tee moi

export PATH="${INSTALL_DIR}/bin:${PATH}"

mkdir -p ${BUILD_ROOT}/gcc/dev
cd ${BUILD_ROOT}/gcc/dev
${GCC_DIR}/configure \
  --target=msp430 \
  --enable-languages=c,c++ \
  --prefix=${INSTALL_DIR} \
  2>&1 | tee co
make 2>&1 | tee mo
make check-gcc RUNTESTFLAGS=msp430.exp
make install 2>&1 | tee moi

cd ${MSP430_LIBC_DIR}/src
rm -rf Build
make PREFIX=${INSTALL_DIR} 2>&1 | tee mo
make PREFIX=${INSTALL_DIR} install 2>&1 | tee moi

cd ${MSP430_MCU_DIR}
export MSP430MCU_ROOT=${MSP430_MCU_DIR}
sh scripts/install.sh ${INSTALL_DIR}

cd ${MSPDEBUG_DIR}
make PREFIX=${INSTALL_DIR} install

cd ${MSP430_GDB_DIR}
patch -p1 < ${SRC_ROOT}/mspgcc/msp430-gdb-7.2a-20111205.patch
mkdir -p $BUILD_ROOT/gdb/dev
cd $BUILD_ROOT/gdb/dev
$MSP430_GDB_DIR/configure --prefix=$INSTALL_DIR \
        --target=msp430 \
        --disable-werror
make | tee mo
make install | tee moi

# Setup wrapper scripts
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/msp430-gcc-wrapper" "msp430-gcc"
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/msp430-ar-wrapper" "msp430-ar"
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/msp430-gdb-wrapper" "msp430-gdb"
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/mspdebug-wrapper" "mspdebug"

# Copy dependencies
#cp -a /usr/lib/libgmp.so* $INSTALL_DIR/lib
#cp -a /usr/lib/libmpc.so* $INSTALL_DIR/lib
#cp -a /usr/lib/libmpfr.so* $INSTALL_DIR/lib

# Create package
mkdir -p $PACKAGE_DIR
cp -R $INSTALL_DIR/* $PACKAGE_DIR
cat > $PACKAGE_DIR/tool.info<<EOF
id=$TOOLCHAIN_ID_PREFIX.$OS.$ARCH.$VERSION
name=$TOOLCHAIN_DESC
os=$OS
arch=$ARCH
toolsProvided=msp430-gdb,mspdebug,msp430-gcc,msp430-ar
mspdebug=bin/mspdebug-wrapper
msp430-gdb=bin/msp430-gdb-wrapper
msp430-gcc=bin/msp430-gcc-wrapper
msp430-ar=bin/msp430-ar-wrapper
EOF

mkdir -p $PACKAGE_DIR/docs/binutils
cp $BINUTILS_DIR/COPYING* $PACKAGE_DIR/docs/binutils
BINUTILS_REV=$(cat $BINUTILS_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/gcc
cp $GCC_DIR/COPYING* $PACKAGE_DIR/docs/gcc
GCC_REV=$(cat $GCC_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/msp430-libc
cp $MSP430_LIBC_DIR/COPYING* $PACKAGE_DIR/docs/msp430-libc
MSP430_LIBC_REV=$(cat $MSP430_LIBC_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/msp430mcu
cp $MSP430_MCU_DIR/COPYING* $PACKAGE_DIR/docs/msp430mcu
#MSP430_MCU_REV=$(cat $MSP430_MCU_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/mspdebug
cp $MSPDEBUG_DIR/COPYING* $PACKAGE_DIR/docs/mspdebug
MSPDEBUG_REV=$(cat $MSPDEBUG_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/gdb
cp $MSP430_GDB_DIR/COPYING* $PACKAGE_DIR/docs/gdb

MSPGCC_REV=$(cat $MSPGCC_DIR/git-version)

cat > $PACKAGE_DIR/README<<EOF
This package is build from the following pieces of software:
$GDB_SOURCE
$MSP430_MCU_SOURCE
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/binutils $BINUTILS_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/gcc $GCC_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/msp430-libc $MSP430_LIBC_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/mspgcc $MSPGCC_REV
git://mspdebug.git.sourceforge.net/gitroot/mspdebug/mspdebug $MSPDEBUG_REV
EOF
