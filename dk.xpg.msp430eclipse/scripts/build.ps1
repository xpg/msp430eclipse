$WORK = "c:\tmp\msp430"
$DOWNLOAD = "$WORK\download"
$VERSION = "3.0"

$TOOLCHAIN_NAME = "msp430-toolchain-win-x86"

mkdir $WORK 2> $null
mkdir $DOWNLOAD 2> $null

$INSTALL = "$WORK\install"
mkdir $INSTALL 2> $null
$INSTALL = "$WORK\install\$TOOLCHAIN_NAME"
mkdir $INSTALL 2> $null

$SRC = "$WORK\src"
mkdir $SRC 2> $null

$env:Path = "$env:Path;c:\mingw\bin;c:\program files (x86)\git\bin"

Function Download ($url, $zip) 
{
	$dst = "$DOWNLOAD\$zip"
	if ( test-path $dst )
	{
		echo "$dst exists"
	} else
	{
		echo "Downloading $url to $zip"
		$client = new-object System.Net.WebClient
		$client.DownloadFile($url, $dst)
	}
}

Function GitUpdate ($url, $dst)
{
	if ( test-path $dst )
	{
		git pull $dst
	} else
	{
		git clone $url $dst
	}

	git --git-dir=$dst\.git rev-parse HEAD > $dst\git-version
}

function UnZipMe($zipfilename,$destination) 
{ 
	$shellApplication = new-object -com shell.application 
	$zipPackage = $shellApplication.NameSpace($zipfilename) 
	$destinationFolder = $shellApplication.NameSpace($destination) 

	# CopyHere vOptions Flag # 4 - Do not display a progress dialog box. 
	# 16 - Respond with "Yes to All" for any dialog box that is displayed. 

	$destinationFolder.CopyHere($zipPackage.Items(),20) 
} 

$MSPDEBUG_GIT = "git://mspdebug.git.sourceforge.net/gitroot/mspdebug/mspdebug"

$MSPGCC_ZIP = "mspgcc-20120406-p20120502.zip"
$MSPGCC_SOURCE = "http://sourceforge.net/projects/mspgcc/files/Windows/mingw32/mspgcc-20120406-p20120502.zip"

$MINGW_REGEX_ZIP="regex-2.7-bin.zip"
$MINGW_REGEX_SOURCE="http://sourceforge.net/projects/gnuwin32/files/regex/2.7/regex-2.7-bin.zip/download"

$MINGW_USB_ZIP="libusb-win32-bin-1.2.5.0.zip"
$MINGW_USB_SOURCE="http://sourceforge.net/projects/libusb-win32/files/libusb-win32-releases/1.2.5.0/libusb-win32-bin-1.2.5.0.zip"

$MINGW_READLINE_ZIP="readline-5.0-1-bin.zip"
$MINGW_READLINE_SOURCE="http://sourceforge.net/projects/gnuwin32/files/readline/5.0-1/readline-5.0-1-bin.zip"

Download $MSPGCC_SOURCE $MSPGCC_ZIP
Download $MINGW_REGEX_SOURCE $MINGW_REGEX_ZIP
Download $MINGW_USB_SOURCE $MINGW_USB_ZIP
Download $MINGW_READLINE_SOURCE $MINGW_READLINE_ZIP

echo "Extracting $MSPGCC_ZIP to $INSTALL"
UnZipMe "$DOWNLOAD\$MSPGCC_ZIP" $INSTALL

echo "Extracting $MINGW_REGEX_ZIP..."
mkdir $SRC\regex 2> $null
UnZipMe "$DOWNLOAD\$MINGW_REGEX_ZIP" "$SRC\regex"
copy "$SRC\regex\include\*.*" c:\mingw\include
copy "$SRC\regex\lib\*.*" c:\mingw\lib
copy "$SRC\regex\bin\*.dll" "$INSTALL\bin"

echo "Extracting $MINGW_USB_ZIP..."
UnZipMe "$DOWNLOAD\$MINGW_USB_ZIP" "$SRC"
copy "$SRC\libusb-win32-bin-1.2.5.0\include\lusb0_usb.h" "c:\mingw\include\usb.h"
copy "$SRC\libusb-win32-bin-1.2.5.0\lib\gcc\libusb.a" "c:\mingw\lib"
copy "$SRC\libusb-win32-bin-1.2.5.0\bin\x86\libusb0_x86.dll" "$INSTALL\bin\libusb0.dll"

echo "Extracting $MINGW_READLINE_ZIP..."
mkdir $SRC\readline 2> $null
UnZipMe "$DOWNLOAD\$MINGW_READLINE_ZIP" "$SRC\readline"
copy -Recurse "$SRC\readline\include\*.*" c:\mingw\include
copy "$SRC\readline\lib\*.*" c:\mingw\lib
copy "$SRC\readline\bin\*.dll" "$INSTALL\bin"

GitUpdate $MSPDEBUG_GIT $SRC\mspdebug
$w = Get-Location
cd $SRC\mspdebug
mingw32-make "PREFIX=$INSTALL" 
copy mspdebug.exe "$INSTALL\bin"
copy ti_3410.fw.ihex "$INSTALL\bin"
cd $w

copy c:\mingw\bin\mingw32-make.exe "$INSTALL\bin"
copy c:\mingw\bin\libintl*.dll "$INSTALL\bin"
copy c:\mingw\bin\libgcc*.dll "$INSTALL\bin"
copy c:\mingw\bin\libiconv*.dll "$INSTALL\bin"
copy c:\mingw\msys\1.0\bin\rm.exe "$INSTALL\bin"
copy c:\mingw\msys\1.0\bin\msys-1.0.dll "$INSTALL\bin"
copy c:\mingw\msys\1.0\bin\msys-iconv-2.dll "$INSTALL\bin"
copy c:\mingw\msys\1.0\bin\msys-intl-8.dll "$INSTALL\bin"

$MSPDEBUG_REV = cat $SRC\mspdebug\git-version

$README="$INSTALL\README.TXT"
$TOOLINFO_TMP="$INSTALL\tool.info.tmp"
$TOOLINFO="$INSTALL\tool.info"

echo "This package is build from the following pieces of software:" > $README
echo "" >> $README
echo "MSPGCC: $MSPGCC_SOURCE" >> $README
echo "regex: $MINGW_REGEX_SOURCE" >> $README
echo "libusb-win32: $MINGW_USB_SOURCE" >> $README
echo "readline: $MINGW_READLINE_SOURCE" >> $README
echo "mspdebug: $MSPDEBUG_GIT $MSPDEBUG_REV" >> $README
echo "MingW: http://www.mingw.org" >> $README

echo "id=dk.xpg.toolchain.windows.$VERSION" > $TOOLINFO_TMP
echo "name=xpg.dk pre-build toolchain $VERSION" >> $TOOLINFO_TMP
echo "os=windows" >> $TOOLINFO_TMP
echo "arch=x86" >> $TOOLINFO_TMP
echo "toolsProvided=msp430-gdb,mspdebug,msp430-gcc,msp430-ar,make" >> $TOOLINFO_TMP
echo "mspdebug=bin/mspdebug" >> $TOOLINFO_TMP
echo "msp430-gdb=bin/msp430-gdb" >> $TOOLINFO_TMP
echo "msp430-gcc=bin/msp430-gcc" >> $TOOLINFO_TMP
echo "msp430-ar=bin/msp430-gcc" >> $TOOLINFO_TMP
echo "make=bin/make.bat" >> $TOOLINFO_TMP

copy make.bat "$INSTALL/bin"

Get-Content $TOOLINFO_TMP | Out-File -Encoding UTF8 $TOOLINFO
del $TOOLINFO_TMP
