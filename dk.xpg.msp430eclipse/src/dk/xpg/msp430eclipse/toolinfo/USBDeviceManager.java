package dk.xpg.msp430eclipse.toolinfo;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.XMLMemento;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.preferences.PreferenceConstants;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class USBDeviceManager {
	
	Map<String, USBDeviceInfo> deviceMap;	 
	IPreferenceStore prefStore;
	
	public USBDeviceManager() {
		
	}
	
	@SuppressWarnings("unchecked")
	private void readDeviceMap() {
		if( prefStore == null) {
			prefStore = MSP430Activator.getDefault().getPreferenceStore();
		}
		
		try {
			InputStream stream = new ByteArrayInputStream(prefStore.getString(PreferenceConstants.P_DEVICE_MAP).getBytes("UTF-8"));
			XMLDecoder decoder= new XMLDecoder(stream);
			deviceMap = (Map<String,USBDeviceInfo>)decoder.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			deviceMap = new HashMap<String, USBDeviceInfo>();
		}		
	}
	
	synchronized public Set<USBDeviceInfo> getUSBDevices() throws IOException, ToolchainNotFoundException {
		if( deviceMap == null) {
			readDeviceMap();
		}
		
		Set<USBDeviceInfo> connectedDevices = getConnectedUSBDevices();
		connectedDevices.addAll(new ArrayList<USBDeviceInfo>(deviceMap.values()));
		return connectedDevices;
	}	
	
	synchronized public Set<USBDeviceInfo> getConnectedUSBDevices(String selectedSerial) throws IOException, ToolchainNotFoundException {
		if( deviceMap == null) {
			readDeviceMap();
		}
		Set<USBDeviceInfo> connectedDevices = getConnectedUSBDevices();
		if( deviceMap.containsKey(selectedSerial) ) {
			USBDeviceInfo selectedDevice = deviceMap.get(selectedSerial);
			connectedDevices.add(selectedDevice);
		}
		return connectedDevices;
	}
	
	synchronized public Set<USBDeviceInfo> getConnectedUSBDevices() throws IOException, ToolchainNotFoundException {
		String toolCommand = MSP430Activator.getDefault().getToolchainManager().getCurrentProvider(ToolType.MSPDEBUG).getExecutable(ToolType.MSPDEBUG);
	
		ProcessBuilder pb = new ProcessBuilder(toolCommand,
				"--usb-list");
		Process p = pb.start();
		
		BufferedReader lineReader = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		for(String s: deviceMap.keySet()) {
			deviceMap.get(s).setConnected(false);
		}
		Set<USBDeviceInfo> deviceList = new HashSet<USBDeviceInfo>();
	
		Pattern descPattern = Pattern.compile("(.*)\\[serial: (.*)\\]");
		
		while(true) {
			String line = lineReader.readLine();
			if( line == null ) { 
				break;
			}
			if( line.startsWith("Devices on")) {
				continue;
			}
			
			line = line.trim();
			String[] fields = line.split(" ", 3);
			if( fields.length < 3 )
				continue;
			
//			String connection = fields[0];
//			String usbId = fields[1];
			String description = "";
			description = fields[2];

			Matcher m = descPattern.matcher(description);
			if( m.matches() ) {
				String type = m.group(1);
				String serial = m.group(2);
								
				USBDeviceInfo device = deviceMap.get(serial); 
				
				if (device == null) {
					device = new USBDeviceInfo(serial);
				}
				device.setConnected(true);
				device.setType(type);
				deviceList.add(device);
			}
		}				
		
		return deviceList;
	}

	public void updateDeviceInfo(USBDeviceInfo deviceInfo) {
		deviceMap.put(deviceInfo.getSerialNumber(), deviceInfo);
	}
	
	public void saveDeviceMap() {
		if( prefStore == null) {
			prefStore = MSP430Activator.getDefault().getPreferenceStore();
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(stream);
		encoder.writeObject(deviceMap);
		encoder.close();
		String s = "";
		try {
			s = stream.toString("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prefStore.setValue(PreferenceConstants.P_DEVICE_MAP, s);
	}
}
