package dk.xpg.msp430eclipse.toolinfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class MSPDebugTargets {
	synchronized public List<String> loadMCUList() throws IOException, ToolchainNotFoundException {
		String toolCommand = MSP430Activator.getDefault().getToolchainManager().getCurrentProvider(ToolType.MSPDEBUG).getExecutable(ToolType.MSPDEBUG);
	
		ProcessBuilder pb = new ProcessBuilder(toolCommand,
				"--fet-list");
		Process p = pb.start();
		
		BufferedReader lineReader = new BufferedReader(new InputStreamReader(
				p.getInputStream()));
		
		/* First line contains some help text, ignore */
		lineReader.readLine();
		
		List<String> mcuList = new ArrayList<String>();
		
		while(true) {
			String line = lineReader.readLine();
			if( line == null ) { 
				break;
			}
			System.err.println(line);
			/* Each line contains a number of entries
			 * separated by spaces */
			StringTokenizer tokenizer = new StringTokenizer(line, " ");
			while(tokenizer.hasMoreTokens()) {
				String mcu = tokenizer.nextToken().toLowerCase();
				System.err.println(mcu);
				mcuList.add(mcu);
			}
		}
		
		Collections.sort(mcuList);
		
		return mcuList;
	}
}
