package dk.xpg.msp430eclipse.toolinfo;

public class USBDeviceInfo {
	private String serialNumber;
	private String name;
	private String type;
	private boolean connected;
	
	public USBDeviceInfo() {
		serialNumber = "";
		name = "";
		type = "";
		connected = false;
	}
	
	public USBDeviceInfo(USBDeviceInfo other) {
		serialNumber = other.serialNumber;
		name = other.name;
		type = other.type;
	}
	
	public USBDeviceInfo(String serialNumber) {
		this();
		this.serialNumber = serialNumber;
	}
	
	public USBDeviceInfo(String serialNumber, String name) {
		this(serialNumber);
		this.name = name;
	}
	
	public String getSerialNumber() {
		return serialNumber;
	}
	
	public void setSerialNumber(String s) {
		serialNumber = s;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String s) {
		name = s;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String s) {
		type = s;
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public void setConnected(boolean connected) {
		this.connected = connected;
	}
}
