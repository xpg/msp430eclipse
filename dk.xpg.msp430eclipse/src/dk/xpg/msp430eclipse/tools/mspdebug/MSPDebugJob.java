package dk.xpg.msp430eclipse.tools.mspdebug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.eclipse.ui.console.MessageConsole;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class MSPDebugJob extends Job {
	private MSPDebug mspDebug;
	private List<String> errorOut;
	private IProject project;

	private static final String NEWLINE = System.getProperty("line.separator");

	public MSPDebugJob(String name, IProject project, MSPDebug mspDebug) {
		super(name);
		this.mspDebug = mspDebug;
		this.project = project;
	}
	
	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		IStatus status = Status.OK_STATUS;
		MSPDebugProcess p = new MSPDebugProcess(mspDebug);
		
		monitor.subTask("Building");
		try {
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, monitor);
			status = p.run(monitor);
		} catch (CoreException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"Build Failed", e);
		}
		
		return status;
		/*IStatus status = Status.OK_STATUS;
		MessageConsole console = MSP430Activator.getDefault().getConsole("MSPDebug");
		console.clearConsole();
		console.activate();
		final IOConsoleOutputStream consoleStream = console.newOutputStream();

		try {
			monitor.subTask("Building");
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, monitor);
			errorOut = new LinkedList<String>();
			monitor.beginTask("Launching MSPDebug", IProgressMonitor.UNKNOWN);
			Process process = mspDebug.launch();
			BufferedReader errorReader = new BufferedReader(
					new InputStreamReader(process.getErrorStream()));
			BufferedReader outputReader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			Thread errorReaderThread = new ReaderThread(errorReader,
					new LineHandler() {
						@Override
						public void handle(String line) {
							errorOut.add(line);
							try {
								consoleStream.write(line);
								consoleStream.write(NEWLINE);
							} catch (IOException e) {
							}
						}
					});
			Thread outputReaderThread = new ReaderThread(outputReader,
					new LineHandler() {

						@Override
						public void handle(String line) {
							try {
								consoleStream.write(line);
								consoleStream.write(NEWLINE);
							} catch (IOException e) {
							}

						}

					});
			errorReaderThread.start();
			outputReaderThread.start();
			while (true) {
				try {
					process.waitFor();
					break;
				} catch (InterruptedException e) {
					// Ignore
				}
			}

			try {
				errorReaderThread.join();
				outputReaderThread.join();
			} catch (InterruptedException e) {
			}

			if (process.exitValue() != 0) {
				MSPDebugErrorParser errorParser = new MSPDebugErrorParser(
						errorOut);
				status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
						errorParser.getErrorMessage());
			}
		} catch (MSPDebugLaunchException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"Failed to launch MSPDebug: " + e.getMessage());
		} catch (IOException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"I/O Error", e);
		} catch (CoreException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID, "Build failed", e);
		} catch (ToolchainNotFoundException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID, "Could not locate mspdebug", e);
		} catch (URISyntaxException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID, "URISyntaxException", e);
		} finally {
			try {
				consoleStream.close();
			} catch (IOException e) {
			}
			monitor.done();
		}
		return status;*/
	}

	private interface LineHandler {
		public void handle(String line);

	}

	private class ReaderThread extends Thread {
		private BufferedReader reader;
		private LineHandler lineHandler;

		public ReaderThread(BufferedReader reader, LineHandler lineHandler) {
			this.reader = reader;
			this.lineHandler = lineHandler;
		}

		@Override
		public void run() {
			try {
				while (true) {
					String line = reader.readLine();
					if (line == null) {
						break;
					}

					lineHandler.handle(line);
				}
				reader.close();
			} catch (IOException e) {

			}
		}
	}
}
