package dk.xpg.msp430eclipse.tools.mspdebug;

import java.util.List;

/**
 * Class used to parse stderr from mspdebug in order to determine the error cause and produce
 * an easy to use error message.
 * @author Paul Fleischer <paul@xpg.dk>
 */
public class MSPDebugErrorParser {
	private List<String> errorOutput;
	
	public MSPDebugErrorParser(List<String> errorOutput) {
		this.errorOutput = errorOutput;
	}
	
	/**
	 * Get the last error message from mspdebug
	 * @return The last line of the errorOutput list
	 */
	public String getErrorMessage() {
		if( errorOutput.size() > 0 )
			return errorOutput.get(errorOutput.size()-1);
		else
			return "";
	}
	
	public Throwable getError() {
		StringBuilder builder = new StringBuilder();
		for(String s: errorOutput) {
			builder.append(s);
			builder.append(System.getProperty("line.separator"));
		}
		return new Throwable(builder.toString());
	}
}
