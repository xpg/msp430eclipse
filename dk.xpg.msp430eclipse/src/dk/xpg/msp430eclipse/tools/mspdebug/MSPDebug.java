package dk.xpg.msp430eclipse.tools.mspdebug;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.ui.console.IOConsoleOutputStream;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class MSPDebug {
	
	public enum DriverFlag {
		USB_ACCESS,
		TTY_ACCESS
	}
	
	public enum Driver {
		RF2500("rf2500", DriverFlag.USB_ACCESS),
		OLIMEX("olimex", DriverFlag.TTY_ACCESS,DriverFlag.USB_ACCESS),
		OLIMEX_V1("olimex-v1", DriverFlag.TTY_ACCESS, DriverFlag.USB_ACCESS),
		OLIMEX_ISO("olimex-iso", DriverFlag.USB_ACCESS),
		OLIMEX_ISO_MK2("olimex-iso-mk2", DriverFlag.TTY_ACCESS, DriverFlag.USB_ACCESS),
		SIM("sim"),
		UIF("uif", DriverFlag.TTY_ACCESS,DriverFlag.USB_ACCESS),
		UIF_BSL("uif-bsl", DriverFlag.TTY_ACCESS),
		FLASH_BSL("flash-bsl", DriverFlag.TTY_ACCESS),
		GDBC("gdbc"),
		TILIB("tilib", DriverFlag.TTY_ACCESS),
		GOODFET("goodfet", DriverFlag.TTY_ACCESS),
		PIF("pif", DriverFlag.TTY_ACCESS);
		
		private final String id;
		private final DriverFlag[] flags;
		
		Driver(String id, DriverFlag... flags) {
			this.id = id;
			this.flags = flags;
		}
		
		public String getId() {
			return id;
		}
		
		public DriverFlag[] getFlags() {
			return flags;
		}
		
		public boolean hasFlag(DriverFlag flag) {
			for(DriverFlag f: flags) {
				if( flag.equals(f)) {
					return true;
				}
			}
			return false;
		}
	}
	
	private Driver driver;
	private boolean useJTAG;
	private String ttyDevice;
	private String usbDevice;
	private String serialNumber;
	private String[] commands;
	
	public MSPDebug() {
		useJTAG = false;
		commands = new String[0];
		serialNumber = null;
	}
	
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	
	public void useJTAG() {
		this.useJTAG = true;
	}
	
	public void useSBW() {
		this.useJTAG = false;
	}
	
	public void setTTYDevice(String ttyDevice) {
		this.ttyDevice = ttyDevice;
		this.usbDevice = null;
	}
	
	public void setUSBDevice(String usbDevice) {
		this.usbDevice = usbDevice;
		this.ttyDevice = null;
	}
	
	public void setCommands(String... commands) {
		this.commands = commands;
	}
	
	public void loadSettings(MSP430PropertyManager props) {
		String driver = props.getPropertyValue("MSPDebugDriver");
		String connection = props.getPropertyValue("MSPDebugConnection");
		String ttyDevice = props.getPropertyValue("MSPDebugTTYDevice");
		String protocol = props.getPropertyValue("MSPDebugProtocol");
		String serialNumber = props.getPropertyValue("DeviceSerialNumber");
		for(Driver d: Driver.values()) {
			if( driver.equals(d.getId())) {
				setDriver(d);
			}
		}
		
		if( connection.equals("TTY") && ttyDevice != null && ttyDevice.length() > 0) {
			setTTYDevice(ttyDevice);
		}
		
		if (protocol != null && protocol.equals("JTAG")) {
			useJTAG();
		} else {
			useSBW();
		}
		
		if( !serialNumber.equals("") ) {
			setSerialNumber(serialNumber);
		}
	}
	
	public Process launch() throws MSPDebugLaunchException, IOException, ToolchainNotFoundException, URISyntaxException {
		if( driver == null ) {
			throw new MSPDebugLaunchException("No driver set");
		}	
		if (!driver.hasFlag(DriverFlag.TTY_ACCESS) && ttyDevice != null) {
			throw new MSPDebugLaunchException("TTY Device specified, but driver " + driver.name() + " does not support TTY");
		}
		if (!driver.hasFlag(DriverFlag.USB_ACCESS) && usbDevice != null) {
			throw new MSPDebugLaunchException("USB Device specified, but driver " + driver.name() + " does not support USB");
		}
		if( commands.length == 0) {
			throw new MSPDebugLaunchException("No commands given");
		}
		
		List<String> argumentList = new ArrayList<String>();

		argumentList.add(MSP430Activator.getDefault().getMSPDebug());
		
		argumentList.add(driver.getId());
		if( useJTAG) {
			argumentList.add("-j");
		}
		if( ttyDevice != null) {
			argumentList.add("-d");
			argumentList.add(ttyDevice);
		}
		if( usbDevice != null) {
			argumentList.add("-U");
			argumentList.add(usbDevice);
		}
		
		if( serialNumber != null) {
			argumentList.add("-s");
			argumentList.add(serialNumber);
		}
		
		for(String c: commands) {
			argumentList.add(c);
		}
		ProcessBuilder builder = new ProcessBuilder(argumentList);

		 IOConsoleOutputStream out = MSP430Activator.getDefault().getConsole(
					"MSPDebug").newOutputStream();
		 out.write("Args: " + argumentList + "\n");
		
		return builder.start();		
	}
}
