package dk.xpg.msp430eclipse;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.osgi.service.prefs.BackingStoreException;

public class MSP430PropertyManager {
	private static Map<IProject,MSP430PropertyManager> instances = null;
	private static final String QUALIFIER = MSP430Activator.PLUGIN_ID + "/msp430";
	
	public synchronized static MSP430PropertyManager getInstance(IProject project) {
		if( instances == null) {
			instances = new HashMap<IProject, MSP430PropertyManager>();
		}
		if( !instances.containsKey(project) ) {
			instances.put(project, new MSP430PropertyManager(project));
		}
		return instances.get(project);
	}
	
	private IProject project;
	
	public MSP430PropertyManager(IProject project) {
		this.project = project;
	}
	
	public String getPropertyValue(String name) {
		ProjectScope scope = new ProjectScope(project);
		IEclipsePreferences prefs = scope.getNode(QUALIFIER);
		return prefs.get(name, "");
	}
	
	public void setPropertyValue(String name, String value) {
		ProjectScope scope = new ProjectScope(project);
		IEclipsePreferences prefs = scope.getNode(QUALIFIER);
		prefs.put(name, value);
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
