package dk.xpg.msp430eclipse.toolchain;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractMSP430ToolProvider implements IMSP430ToolProvider {

	private Map<ToolType,String> providerMap;
	private Set<ToolType> types;
	
	public AbstractMSP430ToolProvider() {
		providerMap = init();
		if( providerMap == null) {
			providerMap = new HashMap<ToolType, String>();
		}
		
		types = new HashSet<ToolType>();
		for(ToolType t: providerMap.keySet()) {
			types.add(t);
		}
	}
	
	protected abstract Map<ToolType,String> init();
	
	@Override
	public String getExecutable(ToolType type) {
		return providerMap.get(type);
	}

	@Override
	public Set<ToolType> getTypes() {
		return types;
	}

	@Override
	public String getProviderId() {
		return getClass().getCanonicalName();
	}
}
