package dk.xpg.msp430eclipse.toolchain;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.preference.IPreferenceStore;

import dk.xpg.msp430eclipse.MSP430Activator;

public class SystemToolProvider implements IMSP430ToolProvider {

	Set<ToolType> types;
	
	public SystemToolProvider() {
		types = new HashSet<ToolType>();
		types.add(ToolType.GCC);
		types.add(ToolType.GDB);
		types.add(ToolType.AR);
		types.add(ToolType.MSPDEBUG);
		types.add(ToolType.MAKE);
	}
	
	@Override
	public String getName() {
		return "System Tools";
	}

	@Override
	public Set<ToolType> getTypes() {
		return types;
	}

	@Override
	public String getExecutable(ToolType type) {
		IPreferenceStore store = MSP430Activator.getDefault().getPreferenceStore();
		String prefix = store.getString(getPrefixPreferenceName(type));
		if( prefix.trim().equals("")) {
			return type.getCommandName();
		} else {
			return new File(prefix, type.getCommandName()).getAbsolutePath();
		}
	}
	
	public void setToolPrefix(ToolType type, String prefix) {
		IPreferenceStore store = MSP430Activator.getDefault().getPreferenceStore();
		store.setValue(getPrefixPreferenceName(type), prefix);
	}

	@Override
	public String getProviderId() {
		return getClass().getCanonicalName();
	}

	public static String getPrefixPreferenceName(ToolType t) {
		return "TOOL_PREFIX_" + t.getName();
	}
}
