package dk.xpg.msp430eclipse.toolchain;

import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;

public class ToolchainNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4775639711097832088L;

	private ToolType type;
	
	public ToolchainNotFoundException(ToolType type) {
		this.type = type;
	}
	
	public ToolchainNotFoundException(ToolType type, String msg) {
		super(msg);
		this.type = type;
	}
	
	public ToolType getToolchainType() {
		return type;
	}	
}
