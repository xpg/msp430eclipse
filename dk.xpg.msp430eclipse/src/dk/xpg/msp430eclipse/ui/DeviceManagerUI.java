package dk.xpg.msp430eclipse.ui;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;
import dk.xpg.msp430eclipse.toolinfo.USBDeviceInfo;

public class DeviceManagerUI extends Dialog {

	private Table deviceTable;
	private TableEditor deviceTableEditor;
	private Button activateButton;
	private Button addButton;
	private Button removeButton;
	private Button closeButton;
	private IPreferenceStore prefStore;
	private Shell dialogShell;
		
	public DeviceManagerUI(Shell parent, int style) {
		super(parent, style);
		
	}
	
	public DeviceManagerUI(Shell parent) {
		super(parent, 0);
	}

	public void open() {		
		prefStore = MSP430Activator.getDefault().getPreferenceStore();
		
		Shell parent = getParent();
		dialogShell = new Shell(parent, SWT.DIALOG_TRIM|SWT.APPLICATION_MODAL|SWT.RESIZE);
		dialogShell.setText("Device Manager");
		
		dialogShell.setLayout(new GridLayout(2,false));
		createControls(dialogShell);
		restoreList();
		
		dialogShell.layout();
		
		dialogShell.open();
		
		Display display = parent.getDisplay();
		while(!dialogShell.isDisposed()) {
			if( !display.readAndDispatch())
				display.sleep();
		}
	}
	
	private void createControls(Composite parent) {

		Composite listGroup = new Composite(parent, SWT.NONE);
		listGroup.setLayout(new GridLayout());
		GridData gd = new GridData();
        gd.verticalAlignment = GridData.FILL;
        gd.horizontalAlignment = GridData.FILL;
        gd.horizontalSpan = 1;
        gd.grabExcessHorizontalSpace = true;
        gd.grabExcessVerticalSpace = true;
        listGroup.setLayoutData(gd);
        
        createList(listGroup);
        
        Composite buttonGroup = new Composite(parent, SWT.NONE);
        buttonGroup.setLayout(new GridLayout(1,true));
		gd = new GridData();
        gd.verticalAlignment = GridData.FILL;
        gd.horizontalAlignment = GridData.END;
        gd.horizontalSpan = 1;
        gd.grabExcessVerticalSpace = true;
        buttonGroup.setLayoutData(gd);
        
        createButtons(buttonGroup);
	}
	
	private Composite createList(Composite parent) {
		deviceTable = new Table(parent, SWT.BORDER);
		deviceTable.setHeaderVisible(true);
		deviceTable.setLinesVisible(true);
		
		TableColumn serialColumn = new TableColumn(deviceTable, SWT.NONE);
		serialColumn.setText("Serial Number");
		TableColumn typeColumn = new TableColumn(deviceTable, SWT.NONE);
		typeColumn.setText("Type");		
		TableColumn nameColumn = new TableColumn(deviceTable, SWT.NONE);
		nameColumn.setText("Name");
		
		serialColumn.setWidth(200);
		typeColumn.setWidth(50);
		nameColumn.setWidth(100);
		
		GridData gd = new GridData();
        gd.verticalAlignment = GridData.FILL;
        gd.horizontalAlignment = GridData.FILL;
        gd.horizontalSpan = 1;
        gd.grabExcessHorizontalSpace = true;
        gd.grabExcessVerticalSpace = true;
        deviceTable.setLayoutData(gd);        
        
        deviceTableEditor = new TableEditor(deviceTable);
        deviceTableEditor.horizontalAlignment = SWT.LEFT;
        deviceTableEditor.grabHorizontal = true;
        deviceTableEditor.minimumWidth = 50;
        
        deviceTable.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		Control oldEditor = deviceTableEditor.getEditor();
        		if( oldEditor != null ) {
        			oldEditor.dispose();
        		}
        		
        		TableItem item = (TableItem)e.item;
        		if( item == null) {
        			return;
        		}
        		
        		Text newEditor = new Text(deviceTable, SWT.NONE);
                newEditor.setText(item.getText(2));
                newEditor.addModifyListener(new ModifyListener() {
                  public void modifyText(ModifyEvent me) {
                    Text text = (Text) deviceTableEditor.getEditor();
                    deviceTableEditor.getItem()
                        .setText(2, text.getText());
                  }
                });
                newEditor.selectAll();
                newEditor.setFocus();
                deviceTableEditor.setEditor(newEditor, item, 2);
        	}
        });
        
        return parent;
	}
	
	private Composite createButtons(Composite parent) {
		GridData gd;
				
		removeButton = new Button(parent, SWT.NONE);
		removeButton.setText("Remove");
		gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		removeButton.setLayoutData(gd);
		
		removeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
//				int item = toolsTable.getSelectionIndex();
//				if( item > -1 ) {
//					GenericToolProvider tp = (GenericToolProvider)toolsTable.getItem(item).getData();
//					MSP430Activator.getDefault().getToolchainManager().removeToolProvider(tp);
//					
//					toolsTable.remove(item);
//					saveList();
//				}
			}
		});
		
		closeButton = new Button(parent, SWT.NONE);
		closeButton.setText("Close");
		gd = new GridData();
		gd.grabExcessVerticalSpace = true;
		gd.verticalAlignment = SWT.END;
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		closeButton.setLayoutData(gd);
		
		closeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				saveList();
				dialogShell.close();
			}
		});
		
		return parent;
	}
	
	private void restoreList() {
		try {
			Collection<USBDeviceInfo> devices = MSP430Activator.getDefault().getUSBDeviceManager().getUSBDevices();
			for(USBDeviceInfo d: devices) {
				TableItem t = new TableItem(deviceTable, SWT.NONE);
				t.setText(new String[]{d.getSerialNumber(), d.getType(), d.getName()});
				t.setData(d);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ToolchainNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		StringTokenizer tok = new StringTokenizer(prefStore.getString(PreferenceConstants.P_TOOL_DIRECTORIES), ";");
//		while(tok.hasMoreTokens()) {
//			addDirectory(tok.nextToken());
//		}
	}
	
	private void saveList() {
//		StringBuilder builder = new StringBuilder();
		for(int i=0; i<deviceTable.getItemCount(); i++) {
			USBDeviceInfo deviceInfo = (USBDeviceInfo)deviceTable.getItem(i).getData();
			deviceInfo.setName(deviceTable.getItem(i).getText(2));
			MSP430Activator.getDefault().getUSBDeviceManager().updateDeviceInfo(deviceInfo);
		}
		MSP430Activator.getDefault().getUSBDeviceManager().saveDeviceMap();
//			
//			if( i < toolsTable.getItemCount()-1) {
//				builder.append(";");
//			}
//		}
//		
//		prefStore.setValue(PreferenceConstants.P_TOOL_DIRECTORIES, builder.toString());
	}
}
