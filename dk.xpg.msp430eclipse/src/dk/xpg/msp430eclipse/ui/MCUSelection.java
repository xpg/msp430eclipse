package dk.xpg.msp430eclipse.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;

import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;
import dk.xpg.msp430eclipse.toolinfo.Targets;

public class MCUSelection  {
	private List categoryList;
	private List mcuList;
	private java.util.List<SelectionListener> selectionListeners;
	private Group group;
	
	public Group getTop() {
		return group;
	}
	
	public MCUSelection(Composite parent) {
		group = new Group(parent, SWT.BORDER_SOLID);
		
		group.setText("Target MCU");
		group.setLayout(new GridLayout(2, false));
		/*Label label = new Label(group, SWT.NONE);
		label.setText("Target MCU:");*/
		GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
		gridData.heightHint = 200;
		/*label.setLayoutData(gridData);*/

		categoryList = new List(group, SWT.BORDER|SWT.SINGLE|SWT.V_SCROLL|SWT.H_SCROLL);
		categoryList.setLayoutData(gridData);
		
		try {
			java.util.List<String> categories = new ArrayList<String>(Targets.getInstance().getCategories());
			Collections.sort(categories);
			for(String c: categories) {
				categoryList.add(c);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
				
		mcuList = new List(group, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL
				| SWT.H_SCROLL);
		gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
		gridData.heightHint = 200; /* this is ugly, but it works */
		gridData.widthHint = 200;
		mcuList.setLayoutData(gridData);
			
		mcuList.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				arg0.text = mcuList.getSelection()[0];
				for(SelectionListener s: selectionListeners) {
					s.widgetSelected(arg0);
				}				
			}
			
		});
		categoryList.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateMCUList();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {	
			}
		});
		
		selectionListeners = new ArrayList<SelectionListener>();
	}

	private void updateMCUList() {
		int selectedCategory = categoryList.getSelectionIndex();
		String category = categoryList.getItem(selectedCategory);
		java.util.List<String> values = Targets.getInstance().getMCUList(category);
		mcuList.setItems(values.toArray(new String[0]));
	}

	public void addSelectionListener(SelectionListener s) {
		selectionListeners.add(s);
	}
	
	public void removeSelectionListener(SelectionListener s) {
		selectionListeners.remove(s);
	}
	
	public String getSelection() {
		if( mcuList.getSelection().length == 0)
			return "";
		return  mcuList.getSelection()[0];		
	}
	
	public void setSelection(String value) throws IOException, ToolchainNotFoundException {
		String category = Targets.getInstance().getCategory(value);
		if( category == null ) {
			categoryList.setSelection(0);
		} else {
			categoryList.setSelection(categoryList.indexOf(category));
		}
		updateMCUList();
		mcuList.setSelection(mcuList.indexOf(value));
	}
	
}
