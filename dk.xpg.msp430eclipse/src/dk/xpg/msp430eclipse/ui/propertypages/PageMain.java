package dk.xpg.msp430eclipse.ui.propertypages;

import org.eclipse.cdt.ui.newui.AbstractPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class PageMain extends AbstractPage {

	public PageMain() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void contentForCDT(Composite composite) {
		super.contentForCDT(composite);		
	}
		
	@Override
	protected boolean isSingle() {
		return true;
	}
	
	@Override
	public boolean isMultiCfg() {
		return false;
	}
	
	@Override
	protected boolean showsConfig() {
		return false;
	}
}
