package dk.xpg.msp430eclipse.ui.propertypages;

import java.io.IOException;
import java.util.Set;

import org.eclipse.cdt.core.settings.model.ICConfigurationDescription;
import org.eclipse.cdt.core.settings.model.ICResourceDescription;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.cdt.ui.newui.AbstractCPropertyTab;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.managedbuild.ConfigurationMacroNames;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;
import dk.xpg.msp430eclipse.toolinfo.USBDeviceInfo;
import dk.xpg.msp430eclipse.toolinfo.USBDeviceManager;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebug;
import dk.xpg.msp430eclipse.ui.DeviceManagerUI;
import dk.xpg.msp430eclipse.ui.MCUSelection;

public class TabTargetHardware extends AbstractCPropertyTab {
	private List mspDebugDriverList;	
	private Button usbConnectionButton;
	private Button ttyConnectionButton;
	private Text ttyDevice;
	private Combo protocolCombo;
	private MCUSelection mcuSelection;
	private Combo usbDeviceButton; 

	private MSP430PropertyManager getPropertyManager(ICResourceDescription desc) {
		ICConfigurationDescription cfgDes = desc.getConfiguration();
		IConfiguration conf = ManagedBuildManager
				.getConfigurationForDescription(cfgDes);
		IProject project = conf.getOwner().getProject();
		return MSP430PropertyManager.getInstance(project);
	}

	public TabTargetHardware() {

	}

	@Override
	protected void performApply(ICResourceDescription src,
			ICResourceDescription dst) {
		String value = mcuSelection.getSelection();
		getPropertyManager(src).setPropertyValue(
				ConfigurationMacroNames.MSP430TARGETMCU.name(), value);

		if( mspDebugDriverList.getSelection().length > 0) {
			value = mspDebugDriverList.getSelection()[0];
			getPropertyManager(src).setPropertyValue("MSPDebugDriver", value);
		}
		
		String connection = "";
		if( usbConnectionButton.getSelection() && usbConnectionButton.getEnabled()) {
			connection = "USB";
		} else if( ttyConnectionButton.getSelection() && ttyConnectionButton.getEnabled()) {
			connection = "TTY";
		}
		getPropertyManager(src).setPropertyValue("MSPDebugConnection", connection);
		getPropertyManager(src).setPropertyValue("MSPDebugTTYDevice", ttyDevice.getText());
		getPropertyManager(src).setPropertyValue("MSPDebugProtocol", protocolCombo.getText());
		
		if( usbDeviceButton.getData(usbDeviceButton.getText()) != null ) {
			USBDeviceInfo d = (USBDeviceInfo)usbDeviceButton.getData(usbDeviceButton.getText());
			getPropertyManager(src).setPropertyValue("DeviceSerialNumber", d.getSerialNumber());
		} else {
			getPropertyManager(src).setPropertyValue("DeviceSerialNumber", "");
		}
	}

	@Override
	protected void performOK() {		
		performApply(getResDesc(), getResDesc());
	}
	
	@Override
	protected void performDefaults() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void updateData(ICResourceDescription cfg) {
		String value = getPropertyManager(cfg).getPropertyValue(
				ConfigurationMacroNames.MSP430TARGETMCU.name());

		try {
			mcuSelection.setSelection(value);
		} catch (Exception e) {
			e.printStackTrace();
		}

		value = getPropertyManager(cfg).getPropertyValue("MSPDebugDriver");
		mspDebugDriverList.setSelection(mspDebugDriverList.indexOf(value));
		
		ttyDevice.setText(getPropertyManager(cfg).getPropertyValue("MSPDebugTTYDevice"));
		
		value = getPropertyManager(cfg).getPropertyValue("MSPDebugConnection");
			
		if( value.equals("USB")) {
			usbConnectionButton.setSelection(true);
		} else if( value.equals("TTY")){
			ttyConnectionButton.setSelection(true);
		}
		
		value = getPropertyManager(cfg).getPropertyValue("MSPDebugProtocol");
		
		if (value.equals("SBW")) {
			protocolCombo.select(0);
		} else if (value.equals("JTAG")) {
			protocolCombo.select(1);
		}

		value = getPropertyManager(cfg).getPropertyValue("DeviceSerialNumber");
		updateConnectedDevices(value);
//		usbDeviceButton.select(0);
//		for(int i=0; i<usbDeviceButton.getItemCount(); i++) {
//			String item = usbDeviceButton.getItem(i);
//			Object o = usbDeviceButton.getData(item);
//			if( o != null ) {
//				USBDeviceInfo deviceInfo = (USBDeviceInfo)o;
//				if( deviceInfo.getSerialNumber().equals(value)) {
//					usbDeviceButton.select(i);
//					break;
//				}
//			}
//		}
		updateUIState();
	}

	@Override
	protected void updateButtons() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void createControls(Composite parent) {
		super.createControls(parent);
		usercomp.setLayout(new GridLayout(2, false));

		createMCUSelectionControls(usercomp);
		createMSPDebugControls(usercomp);
	}	
	
	private void createMCUSelectionControls(Composite parent) {
		mcuSelection = new MCUSelection(parent);
	}
	
	private void createMSPDebugControls(Composite parent) {
		Group group = new Group(parent, SWT.BORDER_SOLID);
		group.setLayout(new GridLayout(2, false));
		group.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true, 2, 1));
		group.setText("MSPDebug");		
		Label label = new Label(group, SWT.NONE);
		label.setText("Driver: ");
		label.setLayoutData(new GridData(SWT.LEAD, SWT.TOP, false, false));

		mspDebugDriverList = new List(group, SWT.BORDER | SWT.SINGLE
				| SWT.V_SCROLL | SWT.H_SCROLL);
		GridData gridData = new GridData();
		gridData.heightHint = 100; /* this is ugly, but it works */
		mspDebugDriverList.setLayoutData(gridData);

		for (MSPDebug.Driver d : MSPDebug.Driver.values()) {
			mspDebugDriverList.add(d.getId());
		}

		mspDebugDriverList.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				updateUIState();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

		});
		
		createMSPDebugDeviceSelection(group);			
	}

	private void updateUIState() {
		if( mspDebugDriverList.getSelection().length == 0)
			return;
		String value = mspDebugDriverList.getSelection()[0];
		for (MSPDebug.Driver d : MSPDebug.Driver.values()) {
			if (value.equals(d.getId())) {
				updateMSPDebugConnectionAvailability(d.hasFlag(MSPDebug.DriverFlag.TTY_ACCESS), d.hasFlag(MSPDebug.DriverFlag.USB_ACCESS));
				break;
			}
		}
	}
	
	private void updateConnectedDevices(String selectSerial) {
		usbDeviceButton.removeAll();
		try {
			USBDeviceManager deviceManager = MSP430Activator.getDefault().getUSBDeviceManager();
			Set<USBDeviceInfo> devices = deviceManager.getConnectedUSBDevices(selectSerial);
			
			usbDeviceButton.add("Automatic");
			usbDeviceButton.select(0);
			for(USBDeviceInfo d: devices) {
				String title = d.getSerialNumber();
				if( !d.getName().equals("") ) {
					title = title + " " + d.getName();
				}
				if( !d.isConnected() ) {
					title = "NC " + title;
				}
				usbDeviceButton.add(title);
				usbDeviceButton.setData(title, d);
				if( d.getSerialNumber().equals(selectSerial)) {
					usbDeviceButton.select(usbDeviceButton.getItemCount()-1);
				}
			}
			usercomp.pack();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ToolchainNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void createMSPDebugDeviceSelection(Composite parent) {
		Label label = new Label(parent, SWT.NONE);
		label.setText("Connection:");
		label.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		
		Composite group = new Composite(parent, SWT.NONE);
		group.setLayout(new GridLayout(3, false));
		usbConnectionButton = new Button(group, SWT.RADIO);
		usbConnectionButton.setText("USB");
		usbConnectionButton.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		//usbDevice = new Text(group, SWT.BORDER);
		usbDeviceButton = new Combo(group, SWT.READ_ONLY);
		usbDeviceButton.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		Button manageDeviceNamesButton = new Button(group, SWT.PUSH);
		manageDeviceNamesButton.setText("Manage device names");
		manageDeviceNamesButton.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		manageDeviceNamesButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				DeviceManagerUI m = new DeviceManagerUI(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
				m.open();
				USBDeviceInfo deviceInfo = (USBDeviceInfo) usbDeviceButton.getData(usbDeviceButton.getText());
				String serial = null;
				if( deviceInfo != null ) {
					serial = deviceInfo.getSerialNumber();
				}
				updateConnectedDevices(serial);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Nothing
			}
		});			
		
		ttyConnectionButton = new Button(group, SWT.RADIO);
		ttyConnectionButton.setText("TTY:");		
		ttyDevice = new Text(group, SWT.BORDER);
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 150;
		gridData.verticalSpan = 2;
		ttyDevice.setLayoutData(gridData);
		
		label = new Label(parent, SWT.NONE);
		label.setText("Protocol:");
		label.setLayoutData(new GridData());
		
		protocolCombo = new Combo(parent, SWT.READ_ONLY);
		protocolCombo.add("SBW");
		protocolCombo.add("JTAG");
		protocolCombo.select(0);
	}	

	private void updateMSPDebugConnectionAvailability(boolean hasTTY, boolean hasUSB) {		
		ttyConnectionButton.setEnabled(hasTTY);
		ttyDevice.setEnabled(hasTTY);
		usbConnectionButton.setEnabled(hasUSB);
	}
	
	
}
