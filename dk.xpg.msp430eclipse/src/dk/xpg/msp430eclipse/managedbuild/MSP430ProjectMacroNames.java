package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.core.resources.IProject;

import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;

public enum MSP430ProjectMacroNames {
	MSP430TARGETMCU(new PropertyResolver()),
	MSP430GCC(new ToolchainResolver(ToolType.GCC)),
	MSP430GDB(new ToolchainResolver(ToolType.GDB)),
	MSP430AR(new ToolchainResolver(ToolType.AR)),
	MSPDEBUG(new ToolchainResolver(ToolType.MSPDEBUG)),
	MSP430MAKE(new ToolchainResolver(ToolType.MAKE));
	
	private MacroResolver resolver;
	private String name;
	
	MSP430ProjectMacroNames(MacroResolver resolver) {
		this.resolver = resolver;
		this.name = name();
	}
	
	MSP430ProjectMacroNames(MacroResolver resolver, String n) {
		this.resolver = resolver;
		this.name = n;
	}
	
	public String resolve(IProject project) {
		return resolver.resolveMacro(project, name());
	}
	
	public String getName() {
		return this.name;
	}
}

class PropertyResolver implements MacroResolver {

	public String resolveMacro(IProject project, String name) {
		MSP430PropertyManager propManager = MSP430PropertyManager.getInstance(project);
		return propManager.getPropertyValue(name);
	}
}