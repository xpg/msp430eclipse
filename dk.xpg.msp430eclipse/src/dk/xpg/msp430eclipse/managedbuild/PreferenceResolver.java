package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.core.resources.IProject;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.preferences.PreferenceConstants;

public class PreferenceResolver implements MacroResolver {

	private String preferenceName;
	
	public PreferenceResolver(String name) {
		this.preferenceName = name;
	}
	
	@Override
	public String resolveMacro(IProject project, String name) {		
		return MSP430Activator.getDefault().getPreferenceStore().getString(preferenceName);
	}

}
