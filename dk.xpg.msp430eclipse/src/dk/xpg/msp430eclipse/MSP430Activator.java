package dk.xpg.msp430eclipse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import dk.xpg.msp430eclipse.preferences.PreferenceConstants;
import dk.xpg.msp430eclipse.toolchain.GenericToolProvider;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolManager;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;
import dk.xpg.msp430eclipse.toolinfo.USBDeviceManager;

/**
 * The activator class controls the plug-in life cycle
 */
public class MSP430Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "dk.xpg.msp430eclipse"; //$NON-NLS-1$

	// The shared instance
	private static MSP430Activator plugin;

	/*private File binaryDirectory;
	private File nativeBinaryDirectory;*/
	private Map<String, String> binaryMap;	
	private ToolManager toolchainManager;
	private USBDeviceManager usbDeviceManager;
	
	/**
	 * The constructor
	 */
	public MSP430Activator() {
		binaryMap = new HashMap<String, String>();
		toolchainManager = new ToolManager(this);
		usbDeviceManager = new USBDeviceManager();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor("dk.xpg.msp430eclipse.native.toolchain");
		for(IConfigurationElement e: config) {
			Object o = e.createExecutableExtension("class");
			if(o instanceof IMSP430ToolProvider) {
				
				IMSP430ToolProvider toolchain = (IMSP430ToolProvider)o;
				toolchainManager.registerToolProvider(toolchain);
			}
		}
		
		StringTokenizer tok = new StringTokenizer(getPreferenceStore().getString(PreferenceConstants.P_TOOL_DIRECTORIES), ";");
		while(tok.hasMoreTokens()) {
			String path = tok.nextToken();
			
			try {
				GenericToolProvider tp = new GenericToolProvider(path);
				toolchainManager.registerToolProvider(tp);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	
	}

	public ToolManager getToolchainManager() {
		return toolchainManager;
	}
	
	public USBDeviceManager getUSBDeviceManager() {
		return usbDeviceManager;
	}
		
	public String getTool(ToolType type) throws ToolchainNotFoundException {
		IMSP430ToolProvider provider = toolchainManager.getCurrentProvider(type);
		return provider.getExecutable(type);
	}
	
	public String getMSPDebug() throws ToolchainNotFoundException, IOException, URISyntaxException {
		return getTool(ToolType.MSPDEBUG);
	}
	
	private void copyURLtoFile(URL source, File destination) throws IOException {		
		InputStream in = source.openStream();
		destination.createNewFile();
		FileOutputStream out = new FileOutputStream(destination); 
		
		final int bufferSize = 8048;
		byte[] buffer = new byte[bufferSize];
		
		while(true) {
			int count = in.read(buffer);
			if( count == -1) {
				break;
			}
			out.write(buffer, 0, count);
		}
				
		in.close();
		out.close();
		
		destination.setExecutable(true);
	}
	
	private void recursiveDelete(File file) {
		if( file.isDirectory() ) {
			File[] children = file.listFiles();
			for(File child: children) {
				recursiveDelete(child);
			}
		}
		file.delete();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static MSP430Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	
	/**
	 * Gets the console with the given name.
	 * <p>
	 * This is a convenience method to get a console with the given name.
	 * </p>
	 * <p>
	 * If the console already exists, a reference is returned, otherwise a new
	 * <code>MessageConsole</code> with the given name is created, added to the
	 * ConsoleManager, and returned.
	 * </p>
	 * 
	 * @param name
	 *            The name of the Console
	 * @return A <code>MessageConsole</code> with the given name
	 */
	public MessageConsole getConsole(String name) {
		// Taken from AVR eclipse
		// Get a list of all known Consoles from the Global Console Manager and
		// see if a Console with the given name already exists.
		IConsoleManager conman = ConsolePlugin.getDefault().getConsoleManager();
		IConsole[] allconsoles = conman.getConsoles();
		for (IConsole console : allconsoles) {
			if (console.getName().equals(name)) {
				return (MessageConsole) console;
			}
		}
		// Console not found - create a new one
		MessageConsole newconsole = new MessageConsole(name, null);
		conman.addConsoles(new IConsole[] { newconsole });
		return newconsole;
	}

}
