package dk.xpg.msp430eclipse.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.internal.ui.workingsets.BuildJob;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.core.IManagedBuildInfo;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.cdt.managedbuilder.macros.BuildMacroException;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacroProvider;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IPageListener;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveListener;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.jface.dialogs.MessageDialog;

import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.natures.MSP430ProjectNature;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebug;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebugJob;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebugLaunchException;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebugProcess;

public class UploadAction implements IWorkbenchWindowActionDelegate,
		IObjectActionDelegate {

	private IWorkbenchWindow window;
	/**
	 * The constructor.
	 */
	public UploadAction() {
		
	}

	/**
	 * The action has been activated. The argument of the method represents the
	 * 'real' action sitting in the workbench UI.
	 * 
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		boolean doRun = false;
		IProject project=null;
		
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow wbWindow = wb.getActiveWorkbenchWindow();
		IWorkbenchPage wbPage = wbWindow.getActivePage();
		IEditorPart editor = wbPage.getActiveEditor();
		IEditorInput input = editor.getEditorInput();
		if (input instanceof IFileEditorInput) {
			project = ((IFileEditorInput) input).getFile()
					.getProject();
			if (project != null) {
				try {
					if (MSP430ProjectNature.hasMSP430Nature(project)) {
						doRun = true;
					}
				} catch (CoreException e) {
					return;
				}
			}
		}

		if (doRun) {
			/*if( window == null) {
				return;
			}*/					
			
			IManagedBuildInfo buildInfo = ManagedBuildManager.getBuildInfo(project);
			IConfiguration activeConfiguration = buildInfo.getDefaultConfiguration();

			MSPDebug mspDebug = new MSPDebug();
			mspDebug.loadSettings(MSP430PropertyManager.getInstance(project));
			IPath builderPath = activeConfiguration.getBuildData().getBuilderCWD();
			IPath outputFile = builderPath.append(activeConfiguration.getArtifactName()).addFileExtension(activeConfiguration.getArtifactExtension());
			String output = outputFile.toString();
			String a = null;
			try {
				a = ManagedBuildManager.getBuildMacroProvider().resolveValue(output, "", ",", IBuildMacroProvider.CONTEXT_CONFIGURATION, activeConfiguration);
			} catch (BuildMacroException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			mspDebug.setCommands("prog "+a);
			
			try {
				project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			
			MSPDebugJob job = new MSPDebugJob("Upload", project, mspDebug);
			job.setUser(true);
			job.schedule();
			/*System.err.println("Run MSPdebug");
			MessageBox box = new MessageBox(window.getShell(), SWT.ICON_INFORMATION);
			box.setMessage("RUnning MSPDebug to upload");
			box.open();*/
		} else {
			System.err.println("No MSP430eclipse project selected");
		}
	}

	/**
	 * Selection in the workbench has been changed. We can change the state of
	 * the 'real' action here if we want, but this can only happen after the
	 * delegate has been created.
	 * 
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	/**
	 * We can use this method to dispose of any system resources we previously
	 * allocated.
	 * 
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to be able to provide parent shell
	 * for the message dialog.
	 * 
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}
}