package dk.xpg.msp430eclipse.debug;

import org.eclipse.cdt.debug.core.ICDTLaunchConfigurationConstants;
import org.eclipse.cdt.dsf.debug.service.command.ICommandControlService.ICommandControlShutdownDMEvent;
import org.eclipse.cdt.dsf.gdb.launching.GdbLaunch;
import org.eclipse.cdt.dsf.gdb.launching.GdbLaunchDelegate;
import org.eclipse.cdt.dsf.service.DsfServiceEventHandler;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebug;
import dk.xpg.msp430eclipse.tools.mspdebug.MSPDebugProcess;

public class LaunchConfigurationDelegate extends GdbLaunchDelegate implements
		ILaunchConfigurationDelegate {

	private MSPDebugProcess mspdebugProcess; 
	
	public LaunchConfigurationDelegate() {
	}

	public LaunchConfigurationDelegate(boolean requireCProject) {
		super(requireCProject);
	}

	@Override
	public void launch(ILaunchConfiguration config, String mode,
			ILaunch launch, IProgressMonitor monitor) throws CoreException {
		
		String projectName = config.getAttribute(ICDTLaunchConfigurationConstants.ATTR_PROJECT_NAME, "");

		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		
		MSPDebug mspDebug = new MSPDebug();
		mspDebug.loadSettings(MSP430PropertyManager.getInstance(project));
		mspDebug.setCommands("gdb 2000");
		GdbLaunch gdbLaunch = (GdbLaunch)launch;
		gdbLaunch.getSession().addServiceEventListener(this, null);
		
		try {
			mspdebugProcess = new MSPDebugProcess(mspDebug);
			mspdebugProcess.runBackground("Bound to port 2000. Now waiting for connection...");
			super.launch(config, mode, launch, monitor);
		} catch (Exception e) {
			abort("Failed to start debug session", e, 42);
		}						
	}		
	
    @DsfServiceEventHandler public void eventDispatched(ICommandControlShutdownDMEvent event) {
    	mspdebugProcess.stop();
    }

}
