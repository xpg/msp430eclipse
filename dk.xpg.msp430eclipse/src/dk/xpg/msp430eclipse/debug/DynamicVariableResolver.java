package dk.xpg.msp430eclipse.debug;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.core.variables.IDynamicVariableResolver;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class DynamicVariableResolver implements IDynamicVariableResolver {

	@Override
	public String resolveValue(IDynamicVariable variable, String argument)
			throws CoreException {
		if( variable.getName().equals("dk.xpg.msp430eclipse.variable.debugger") ) {
			try {
				return MSP430Activator.getDefault().getTool(ToolType.GDB);
			} catch (ToolchainNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
